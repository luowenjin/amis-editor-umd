const path = require('path')
const CompressionPlugin = require("compression-webpack-plugin")
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

module.exports = {
  productionSourceMap: false,
  pages: {
    index: {
      entry: 'examples/main.js',
      template: 'public/index.html',
      filename: 'index.html'
    }
  },
  chainWebpack: config => {

    if (process.env.BUILD_TARGET === "lib") {
      config.plugin('compressionPlugin')
        .use(new CompressionPlugin({
          algorithm: 'gzip',//开启gzip
          test: /\.js$|\.html$|\.css/, // 匹配文件名
          threshold: 10240, // 对超过10k的数据压缩
          deleteOriginalAssets: false // 不删除源文件
        }))
      config.plugins.push(
        new UglifyJsPlugin({
          uglifyOptions: {
            compress: {
              warnings: false,
              drop_debugger: true, // 注释console
              drop_console: true,
              pure_funcs: ['console.log'] // 移除console
            },
          },
          sourceMap: false,
          parallel: true,
        }),
      );
      config.devtool = false,
        config.externals = {
          'vue': 'Vue',
          'axios': 'axios',
          'amis': 'amis',
          'element-ui': 'ELEMENT'
        }
    }
    config.module
      .rule('js')
      .include
      .add(resolve('packages'))
      .end()
      .use('bable')
      .loader('bable-loader')
      .tap(options => {
        return options
      })



  }
}